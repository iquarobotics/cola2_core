#!/usr/bin/env python3
# Copyright (c) 2020 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

import os
import signal
import subprocess
import threading
from typing import Optional

import numpy as np
import rospy
from cola2_msgs.msg import Setpoints
from cola2_msgs.srv import Action, ActionRequest, ActionResponse
from std_srvs.srv import Trigger, TriggerRequest, TriggerResponse

SETPOINTS_RATE = 10.0


class TestThrustersNode:
    """Test thrusters from an Action service."""

    def __init__(self) -> None:
        """Constructor."""
        self.namespace: str = rospy.get_namespace()
        self.frame_id: str = self.namespace[1:] + "safety"
        self.num_thrusters = 3  # changed at the service calls
        self.event_stop = threading.Event()

        # Create publisher
        self.pub_thrusters_data = rospy.Publisher(
            name=rospy.get_namespace() + "controller/thruster_setpoints",
            data_class=Setpoints,
            queue_size=1,
        )

        # Create client to disable thrusters service
        self.disable_thrusters: Optional[rospy.ServiceProxy] = None
        try:
            rospy.wait_for_service(service=rospy.get_namespace() + "controller/disable_thrusters", timeout=10)
            self.disable_thrusters = rospy.ServiceProxy(
                name=rospy.get_namespace() + "controller/disable_thrusters",
                service_class=Trigger,
            )
        except rospy.ServiceException as e:
            rospy.logwarn(f"Service call failed: {e}")
        except rospy.exceptions.ROSException as e:
            rospy.logwarn(f"Wait for service failed: {e}")

        # Create test service
        self.test_srv = rospy.Service(name="~test", service_class=Action, handler=self.test)
        self.test_ramp_srv = rospy.Service(name="~test_ramp", service_class=Action, handler=self.test_ramp)
        self.test_stop_srv = rospy.Service(name="~test_stop", service_class=Trigger, handler=self.test_stop)
        self.rate = rospy.Rate(SETPOINTS_RATE)

    def test_stop(self, req: TriggerRequest) -> TriggerResponse:
        """Set the event flag so that running tests stop at once."""
        self.event_stop.set()
        res = TriggerResponse()
        res.success = True
        return res

    def _send_test(self, msg: Setpoints, duration: float) -> bool:
        """Send setpoints for a certain duration in seconds. Stop motors on ROS shutdown."""
        msg.header.frame_id = self.frame_id
        steps = int(duration * SETPOINTS_RATE)
        for _ in range(steps):
            msg.header.stamp = rospy.Time.now()
            self.pub_thrusters_data.publish(msg)
            self.rate.sleep()
            # manual shutdown
            if self.event_stop.is_set() or rospy.is_shutdown():
                self._send_stop()
                return False  # test cancelled
        return True

    def _send_stop(self) -> None:
        """Set zero setpoints to stop motors."""
        msg = Setpoints()
        msg.header.stamp = rospy.Time.now()
        msg.header.frame_id = self.frame_id
        msg.setpoints = [0.0 for _ in range(self.num_thrusters)]
        self.pub_thrusters_data.publish(msg)

    def test(self, req: ActionRequest) -> ActionResponse:
        """Send setpoints to motor for 3 seconds."""
        # prepare
        self.event_stop.clear()
        if self.disable_thrusters is not None:
            self.disable_thrusters(TriggerRequest())
        self.num_thrusters = len(req.param)

        # prepare message
        msg = Setpoints()
        msg.setpoints = [float(p) for p in req.param]
        rospy.loginfo(f"test {self.num_thrusters} thrusters with setpoints {msg.setpoints}")

        # do the test
        res = ActionResponse()
        res.success = True
        if not self._send_test(msg=msg, duration=3.0):
            res.success = False
            res.message = "test cancelled"

        # service response
        return res

    def test_ramp(self, req: ActionRequest) -> ActionResponse:
        """Request has a number of params equal to the number of motors. Values are not important."""
        # prepare
        self.event_stop.clear()
        if self.disable_thrusters is not None:
            self.disable_thrusters(TriggerRequest())
        self.num_thrusters = len(req.param)
        rospy.loginfo(f"test_ramp with {self.num_thrusters} thrusters")

        # init response
        res = ActionResponse()
        res.success = True

        # warmup
        msg = Setpoints()
        for val in np.arange(0.0, -1.01, -0.1):
            msg.setpoints = [val for _ in range(self.num_thrusters)]
            if not self._send_test(msg=msg, duration=1.0):
                res.success = False
                res.message = "test cancelled"
                return res

        # start recording
        topics = "|".join(
            [
                "/(.*)/adis_imu/(.*)",
                "/(.*)/ahrs_filter/(.*)",
                "/(.*)/controller/thruster_setpoints",
                "/(.*)/diagnostics",
                "/(.*)/emus_bms/(.*)",
                "/(.*)/iqua_t350_thrusters/thrusters_data",
                "/(.*)/seaeye_thrusters/(.*)",
                "/(.*)/setpoints_selector/thruster_setpoints",
                "/rosout",
            ]
        )

        # check bags folder exists
        path_bags = os.path.expanduser("~/bags")
        if not os.path.isdir(path_bags):
            os.makedirs(path_bags)
        BAG_FILENAME_START = "test_thrusters"
        command = f"rosbag record -e '{topics}' -o {BAG_FILENAME_START}"
        bag = subprocess.Popen(
            args=command,
            stdin=subprocess.PIPE,
            shell=True,
            cwd=path_bags,
            preexec_fn=os.setsid,
        )

        # do the ramp test
        for val in np.arange(-1.0, +1.01, 0.1):
            msg.setpoints = [val for _ in range(self.num_thrusters)]
            if not self._send_test(msg=msg, duration=10.0):
                res.success = False
                res.message = "test cancelled"
                break

        # end recording
        os.killpg(os.getpgid(bag.pid), signal.SIGTERM)

        if res.success:
            # stop motors
            self._send_stop()
            # get the most recente bagfile to be returned in the response
            rospy.sleep(0.5)  # allow bag to close
            res.message = os.path.join(
                path_bags,
                self._get_most_recent_file(filename_start=BAG_FILENAME_START, folder=path_bags),
            )
            rospy.loginfo(f"test_ramp succeeded with data in '{res.message}'")

        # service response
        return res

    @staticmethod
    def _get_most_recent_file(filename_start: str, folder: str) -> str:
        candidates = [f for f in os.listdir(folder) if f.startswith(filename_start)]
        candidates.sort()
        if len(candidates) == 0:
            return "not found"  # should never happen
        newest = candidates[-1]
        return newest


if __name__ == "__main__":
    rospy.init_node("test_thrusters")
    node = TestThrustersNode()
    rospy.spin()
